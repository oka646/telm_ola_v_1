class Visit < ApplicationRecord
  belongs_to :patient, :dependent => :delete
  belongs_to :doctor, :dependent => :delete
end
