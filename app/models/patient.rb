class Patient < ApplicationRecord
      validates :name, presence: true
      validates :lname, presence: true
      validates :pesel, presence: true
      validates :dob, presence: true
  has_many :visits, dependent: :delete_all
  has_many :doctors, :through => :visits
end
