class Doctor < ApplicationRecord
    validates :doctor_name, presence: true
    validates :doctor_lname, presence: true
    validates :spec, presence: true
   
    has_many :visits
    has_many :patients, :through =>  :visits
    
  
end
