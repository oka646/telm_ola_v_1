eventCalendar = ->
  $('#calendar').fullCalendar {}

clearCalendar = ->
  $('#calendar').fullCalendar 'delete'
  # In case delete doesn't work.
  $('#calendar').html ''
  return

$(document).on 'turbolinks:load', eventCalendar
$(document).on 'turbolinks:before-cache', clearCalendar

