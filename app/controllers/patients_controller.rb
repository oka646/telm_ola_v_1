class PatientsController < ApplicationController
     
  def new
  end
  
    helper_method :sort_column, :sort_direction
    def index
         @patients = Patient.order(sort_column + " " + sort_direction)
    end
 def create
     
     patient = Patient.new(patient_params)
        
        if patient.save
            redirect_to "/patients"
        else
            flash[:errors] = patient.errors.full_messages
            redirect_to "/patients/new"
        end
 end
     def show
        @patient = Patient.find(params[:id])
     end
     
     def edit
           @patient = Patient.find(params[:id])
     end
      def update
        patient = Patient.find(params[:id])
        if patient.update(patient_params)
            redirect_to "/patients"
        else
            flash[:errors] = patient.errors.full_messages
            redirect_to "/patients/#{patient.id}/edit"
        end
      end
     
     def destroy 
         patient = Patient.find(params[:id])
         patient.destroy
         redirect_to "/patients#index"
     end
 
   def patient_params
    params.require(:patient).permit(:name, :lname, :pesel, :tel, :dob)
   end
   
   private
   def sort_column
       Patient.column_names.include?(params[:sort]) ? params[:sort] : "name"
   end
   
   def sort_direction
       %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
   end
   
end
  

 