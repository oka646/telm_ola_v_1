json.extract! doctor, :id, :doctor_name, :doctor_lname, :spec, :created_at, :updated_at
json.url doctor_url(doctor, format: :json)
