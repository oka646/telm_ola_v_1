json.extract! visit, :id, :start_date, :end_date, :patient_id, :doctor_id, :created_at, :updated_at
json.url visit_url(visit, format: :json)
