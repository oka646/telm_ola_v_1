Rails.application.routes.draw do
  

  resources :visits
  resources :doctors
  root 'static_pages#home'
  get  '/help',   to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/patients',  to: 'patients#index'
  get  '/harmonogram',  to: 'visits#index'
  get '/patients/new', to:  'patients#new'
  post '/patients', to: 'patients#create'
  get '/patients/:id', to: 'patients#show'
  get 'patients/:id/edit', to: 'patients#edit'
  patch "patients/:id" => "patients#update"
  delete "patients/:id" => "patients#destroy"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
