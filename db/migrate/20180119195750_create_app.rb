class CreateApp < ActiveRecord::Migration[5.1]
  def change
    create_table :doctors do |t|
      t.string :doctor_name
      t.string :doctor_lname
      t.string :spec
      t.timestamps
    end
 
    create_table :patients do |t|
      t.string :name
      t.string :lname
      t.integer :pesel
      t.integer :tel
      t.integer :dob
      t.timestamps
    end
 
    create_table :appointments do |t|
      t.belongs_to :doctor, index: true
      t.belongs_to :patient, index: true
      t.datetime :start_date
      t.datetime :end_date
      t.timestamps
    end
  end
end
