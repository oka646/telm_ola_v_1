class DropTables < ActiveRecord::Migration[5.1]
  def change
    drop_table :Patients
    drop_table :Doctors
    drop_table :Visits
  end
end
