class RenameAppointmentsToVisits < ActiveRecord::Migration[5.1]
  def change
    rename_table :appointments, :visits
  end
end
