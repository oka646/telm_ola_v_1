class CreatePatients < ActiveRecord::Migration[5.1]
  def change
    create_table :patients do |t|
      t.string :name
      t.string :lname
      t.integer :pesel
      t.integer :tel
      t.date :dob
      t.timestamps
    end
  end
end
