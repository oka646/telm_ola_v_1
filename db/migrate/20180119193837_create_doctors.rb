class CreateDoctors < ActiveRecord::Migration[5.1]
  def change
    create_table :doctors do |t|
      t.string :doctor_name
      t.string :doctor_lname
      t.string :spec

      t.timestamps
    end
  end
end
